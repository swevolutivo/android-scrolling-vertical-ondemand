package com.example.android.actionbarcompat.basic;

import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pazfernando on 9/28/16.
 */

public class FetchRowsTask extends AsyncTask<Integer, Integer, List<String>> {
    private ArrayAdapter<String> adapter;

    public FetchRowsTask(ArrayAdapter<String> adapter) {
        this.adapter = adapter;
    }

    @Override
    protected void onPreExecute() {
        adapter.add("Loading...");
        adapter.notifyDataSetChanged();
    }

    @Override
    protected List<String> doInBackground(Integer... params) {
        List<String> values = new ArrayList<>();
        System.out.println("Loading.....");
        try {
            synchronized(this){
                wait(3000);
            }
            int first = (params[0]*7)+1;
            for (int i=first; i<first+7; i++) {
//                adapter.add(Integer.toString(i));
                values.add(Integer.toString(i));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return values;
    }

    @Override
    protected void onPostExecute(List<String> result) {
        adapter.addAll(result);
        adapter.remove("Loading...");
        adapter.notifyDataSetChanged();

    }
}
