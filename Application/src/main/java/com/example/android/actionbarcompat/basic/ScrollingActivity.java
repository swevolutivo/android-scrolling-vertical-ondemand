package com.example.android.actionbarcompat.basic;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class ScrollingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        Intent intent = getIntent();
        String message = intent.getStringExtra(EXTRA_MESSAGE);

        ListView myList = (ListView)findViewById(R.id.listSE);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        myList.setAdapter(adapter);
        adapter.add(message);
        adapter.add("2");
        adapter.add("3");
        adapter.add("4");
        adapter.add("5");
        adapter.add("6");
        adapter.add("7");
        myList.setOnScrollListener(new EndlessScrollListener());
        adapter.notifyDataSetChanged();
    }


}
