package com.example.android.actionbarcompat.basic;

import android.os.AsyncTask;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.concurrent.ExecutionException;

/**
 * Created by pazfernando on 9/28/16.
 */

public class EndlessScrollListener implements AbsListView.OnScrollListener {
    private int visibleThreshold = 7;
    private int currentPage = 0;
    private int previousTotal = 0;
    private boolean loading = true;
    private FetchRowsTask fetchRowsTask = null;

    public EndlessScrollListener() {
    }
    public EndlessScrollListener(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (fetchRowsTask == null || !fetchRowsTask.getStatus().equals(AsyncTask.Status.RUNNING)) {
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            System.out.println("totalItemCount->" + totalItemCount + ", visibleItemCount->" + visibleItemCount + ", firstVisibleItem->" + firstVisibleItem + ", visibleThreshold->" + visibleThreshold);
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
                //new LoadGigsTask().execute(currentPage + 1);
                ArrayAdapter<String> adapter = (ArrayAdapter<String>)view.getAdapter();
                fetchRowsTask = new FetchRowsTask(adapter);
                fetchRowsTask.execute(currentPage);
//            try {
//                adapter.addAll(new FetchRowsTask(adapter).execute(currentPage).get());
//                adapter.remove("Loading...");
//                adapter.notifyDataSetChanged();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
                loading = true;
            }

        }
    }
}
